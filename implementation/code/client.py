import socket
import os, time
import commun.client_auth as ca
from commun.myconnection import MyConnection


def erreur():
    print("erreur")
    serveur.send(("erreur").encode())
    serveur.close()
    


hote = input("IP de l'hote : ")

port = int(input("Port : "))

client = input("Nom du client : ")


serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

serveur.connect((hote, port))

print("Connexion avec le serveur sur le port {}".format(port))

serveur = MyConnection(serveur)

try:
    
    #Identifiant de session obtenu grace a Diffie Hellman (obtenu par observation d'une vraie connexion sur Wireshark)
    
    
    h ="2a2b1fe64aa28a165c22e2ce0800450001d8490040003606827189c2025dc0a82ae7001695da826c788b0bc3ffdd801800f9a6a900000101080ae42b3d03644976d8000001040b1f000000680000001365636473612d736861322d6e69737470323536000000086e697374703235360000004104da31ce16f9cb099d012d908083e00bfa940ec8a850a0a206d66855533f3f45893a576af92a5292f037a74925904824cac0981223226c2b5f9c2b3d67de895c09000000209064e0935fc8ab40dde3f9d7ee21b9632e1593025d8a66755a777f5dc582206b000000630000001365636473612d736861322d6e69737470323536000000480000002024aef2a67de1705cfda9e2e9e70ebc1adaa0c22fbc255cc8338b43c1a4711984000000206a30bc3586b14120367ab561045e26515509313193fa34fda8cace26a399971500000000000000000000000000000c0a1500000000000000000000a608750cb266ec8a8f77d2c6dc841f601b52c875fa5119a844a0681cb6104716248dbf3b21b459121935cbd4e9c089803d0a65d700b420eba80d96f149eb717479cce5084abd95c534b0713bcf2f60b217e099a3eef2c3e03e22b06a878d7d0f9de3751bcc4ed119bdbdf86bb45b2636c3e7f20bb2f2f05c5a6d73e1bfb8bdd78ba4de813ce3cd9a15d2d8f5".encode('ascii')
    
    # Authentification 
    clientAuth = ca.Client(serveur, client, h, auth_list=["password","publickey"])
    clientAuth.banner()
    clientAuth.Fillkeylist()
    clientAuth.password_auth()
    ans = clientAuth.auth()
    if ans :
        print("Succesful authentication")
    else:
        print("Failed authentication")
        
    serveur.close()
    
    
except ConnectionAbortedError:
    serveur.dest.close()
    print("Disconnected")
except ConnectionError:
    serveur.dest.close()
    print("Disconnected")

