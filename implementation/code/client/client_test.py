import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
import os
import base64

    
def check_identity(hote,key):
    print("Check identity")
    with open(("known_hosts"), 'r') as content_file:
        list=content_file.read()
    return (hote+" "+key in list)

BLOCK_SIZE = 16

# caractere utilise pour le padding afin que la taille de la chaine envoyee avec aes soit un multiple de BLOCK_SIZE
PADDING = b'{'

# Padding de la chaine a envoyer
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING

EncodeAES = lambda c, s: c.encrypt(pad(s))
DecodeAES = lambda c, e: c.decrypt(e).rstrip(PADDING)

## Client Main



hote = input("IP de l'hote : ")

port = 8000

client = input("Nom du client : ")

with open(("private_"+client+".key"), 'rb') as content_file:
    key=RSA.importKey(content_file.read())

serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

serveur.connect((hote, port))

print("Connexion avec le serveur sur le port {}".format(port))

#Reception de la clef publique du serveur
key_str = (serveur.recv(3000)).decode()
public_serveur_key=RSA.importKey(key_str)

#Verification de l'identite du serveur
if not check_identity(hote, key_str):
    serveur.close()
else:
    aes_key = os.urandom(BLOCK_SIZE)
    aes_key_crypted = public_serveur_key.encrypt(aes_key,1)[0]
    serveur.send(aes_key_crypted)
    aes_cipher = AES.new(aes_key)
    
    encoded = EncodeAES(aes_cipher, (client+" ").encode()+key.exportKey('OpenSSH'))
    serveur.send(encoded)
    
    while True:
    
        message = input('Message a envoyer : ')
        encoded = EncodeAES(aes_cipher, message.encode())
        serveur.send(encoded)
