from Crypto.PublicKey import RSA, DSA
import subprocess

#Création de la paire de clé RSA du client

nom_client= input("Nom du client : ")

process = subprocess.Popen(["mkdir", "keys/"+nom_client], stderr = subprocess.PIPE)
process.communicate()

key_RSA = RSA.generate(2048)
with open(("keys/"+nom_client+"/id_rsa"), 'wb') as content_file:
    content_file.write(key_RSA.exportKey('PEM'))
pubkey_RSA = key_RSA.publickey()
with open(("keys/"+nom_client+"/id_rsa.pub"), 'wb') as content_file:
    content_file.write(pubkey_RSA.exportKey('OpenSSH'))
    
#Création de la paire de clé DSA du client


key_DSA = DSA.generate(2048)
with open(("keys/"+nom_client+"/id_dsa"), 'wb') as content_file:
    content_file.write(key_DSA.exportKey('PEM'))
pubkey_DSA = key_DSA.publickey()
with open(("keys/"+nom_client+"/id_dsa.pub"), 'wb') as content_file:
    content_file.write(pubkey_DSA.exportKey('OpenSSH'))
    
#Stockage de la clé publique dans la liste des clients autorisés

with open(("../serveur/authorized_keys"), 'ab') as content_file:
    content_file.write(pubkey_RSA.exportKey('OpenSSH'))
    content_file.write(b'\n')
    content_file.write(pubkey_DSA.exportKey('OpenSSH'))
    content_file.write(b'\n')
    
