import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES


def check_identity(hote,key):
    print("Check identity")
    with open(("keys_database"), 'r') as content_file:
        list=content_file.read()
    return (hote+" "+key in list)

BLOCK_SIZE = 16

# caractere utilise pour le padding afin que la taille de la chaine envoyee avec aes soit un multiple de BLOCK_SIZE
PADDING = b'{'

# Padding de la chaine a envoyer
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING

EncodeAES = lambda c, s: c.encrypt(pad(s))
DecodeAES = lambda c, e: c.decrypt(e).rstrip(PADDING)

## Main Serveur

address = input("Adresse d'ecoute : ")

with open("private.key", 'rb') as content_file:
    key=RSA.importKey(content_file.read())

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.bind((address, 9999))

while True:
        socket.listen(5)
        client, address = socket.accept()
        print("{} connected".format( address ))
        
        # Envoie de la clef publique du serveur au client pour etre authentifie
        client.send(key.exportKey('OpenSSH'))
        
        #Reception de la clef aes
        aes_key_crypted = (client.recv(1000))
        
        aes_key = key.decrypt(aes_key_crypted)
        
        aes_cipher = AES.new(aes_key)
        
        encoded = client.recv(3000)
        client_key = DecodeAES(aes_cipher, encoded)
        print(client_key)
        if not check_identity(address[0], client_key.decode()):
            client.close()
            socket.close()
            break
        else:
            while True:
                encoded = client.recv(3000)
                decoded = DecodeAES(aes_cipher, encoded)
                print("Message recu : "+decoded.decode())
        