from Crypto.Hash import SHA256
import datetime

now = datetime.datetime.now()

username = input ("Username : ")
passwd = input("Password : ").encode("utf8")

with open("passwd_list", "a") as content_file:
    content_file.write(username+" "+SHA256.new(passwd).hexdigest()+" "+now.strftime("%Y-%m-%d")+"\n")
