import sys, time

class MyConnection:
    def __init__(self, dest):
        self.dest = dest
        
    def close(self):
        time.sleep(0.1)
        self.send("Close".encode('ascii'))
        raise ConnectionError
    
    def end(self):
        print("End connection with this client")
        time.sleep(0.1)
        self.send("Close".encode('ascii'))
        raise ConnectionAbortedError
        
    def send(self, s):
        self.dest.send(s)
        
    def recv(self, i):
        s = self.dest.recv(i)
        if s=="Close".encode('ascii'):
            print("Connection closed")
            self.dest.close()
            raise ConnectionAbortedError
        return s
        
        