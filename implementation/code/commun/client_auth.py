if __name__ == "__main__":
    from sshconst import *
else:
    from commun.sshconst import *
    
import socket, sys, subprocess, os, base64, datetime
from Crypto.PublicKey import RSA, DSA
from Crypto.Signature import pkcs1_15, DSS
from Crypto.Hash import SHA256

        
def decode_message(s):
    l=[]
    l.append(s[0])
    if l[0]==SSH_MSG_USERAUTH_REQUEST:
        length = int.from_bytes(s[1:5], 'big')
        l.append(s[5:5+length].decode("utf8"))
        s=s[5+length:]
        
        length = int.from_bytes(s[0:4], 'big')
        l.append(s[4:4+length].decode("ascii"))
        s=s[4+length:]
        
        length = int.from_bytes(s[0:4], 'big')
        l.append(s[4:4+length].decode("ascii"))
        s=s[4+length:]
        
        if l[-1]=="none":
            return l
        
        l.append(s[0]>0)
        
        if l[-2] == "publickey":
            
            length = int.from_bytes(s[1:5], 'big')
            l.append(s[5:5+length].decode("ascii"))
            s=s[5+length:]
        
            length = int.from_bytes(s[0:4], 'big')
            l.append(s[4:4+length].decode("ascii"))
            s=s[4+length:]
        
            length = int.from_bytes(s[0:4], 'big')
            l.append(s[4:4+length].decode("ascii"))
            s=s[4+length:]
            
        elif l[-2] == "password":
            length = int.from_bytes(s[1:5], 'big')
            l.append(s[5:5+length].decode("utf8"))
            s=s[5+length:]
        
            length = int.from_bytes(s[0:4], 'big')
            l.append(s[4:4+length].decode("utf8"))
            s=s[4+length:]
        
        return l
        
        

class Client:
    
    def __init__(self, serveur, username, session_id, auth_list=["publickey", "password"]):
        
        self.auth_list=auth_list
        self.serveur=serveur
        self.username=username
        self.session_id = session_id
        self.auth_usable=[]

        self.current_key = 0
        self.key_list = []
        
    def banner(self):
        s=self.serveur.recv(3000)
        if s[0]!=SSH_MSG_USERAUTH_BANNER:
            print("error banner")
            self.serveur.close()
        else:
            length = int.from_bytes(s[1:5], 'big')
            print(s[5:5+length].decode("utf8"))
        
    def auth(self):
        self.method_none()
        ans=self.answer_none()
        while ans==False and self.auth_usable!=[] and self.auth_list!=[]:
            ans=self.try_auth()
        
        if ans == 2 or ans == False:
            return False
        else:
            return True
        
    
    def try_auth(self):
        # Determine le prochaine technique d'authentification
        type_request=""
        for x in self.auth_list:
            if x in self.auth_usable:
                type_request=x
                break
                
        if type_request=="publickey":
            return self.key_auth()
        elif type_request=="password":
            return self.password_auth()
        elif  type_request=="":
            return 2
        else:
            print("Type d'dentifiction inconnue")
            self.serveur.close()
            
    
    
    def method_none(self):
        

        s=SSH_MSG_USERAUTH_REQUEST.to_bytes(1,'big')
        
        tmp=self.username.encode("utf8")
        s+=len(tmp).to_bytes(4,'big')+tmp
        
        tmp = "ssh-userauth".encode("ascii")
        s+=len(tmp).to_bytes(4,'big')+tmp
        
        tmp = "none".encode("ascii")
        s+=len(tmp).to_bytes(4,'big')+tmp
        
        self.serveur.send(s)
    
    def key_auth(self):
        
        print("Authentification par clef")

        file = open(("client/keys/"+self.username+"/"+self.key_list[self.current_key]+".pub"), 'rb')
    
        pub_file=file.read()
        algorithm_name = "ssh-".encode('ascii')+pub_file[4:7]

        request = SSH_MSG_USERAUTH_REQUEST.to_bytes(1,'big')
        
        tmp = self.username.encode("utf8")
        request+=len(tmp).to_bytes(4,'big')+tmp
        
        tmp = "ssh-userauth".encode("ascii")
        request+=len(tmp).to_bytes(4,'big')+tmp
        
        tmp = "publickey".encode("ascii")
        request+=len(tmp).to_bytes(4,'big')+tmp
        
        a=0
        request+= a.to_bytes(1,'big')
        
        request+= len(algorithm_name).to_bytes(4,'big')+algorithm_name
        
        request+= len(pub_file).to_bytes(4,'big')+pub_file
        
        self.serveur.send(request)
        
        l = self.answer_publickey()
        
        if l==SSH_MSG_USERAUTH_FAILURE:
            self.current_key = self.current_key+1
            if self.current_key >= len(self.key_list):
                self.auth_list.remove("publickey")
            return False
        elif l!=SSH_MSG_USERAUTH_PK_OK:
            print("erreur: Mauvaise reponse")
            print(l)
            self.serveur.close()
            
        s = SSH_MSG_USERAUTH_REQUEST.to_bytes(1,'big')
        
        tmp = self.username.encode("utf8")
        s+=len(tmp).to_bytes(4,'big')+tmp
        tmp = "ssh-userauth".encode("ascii")
        s+=len(tmp).to_bytes(4,'big')+tmp
        tmp = "publickey".encode("ascii")
        s+=len(tmp).to_bytes(4,'big')+tmp
        a=1
        s+= a.to_bytes(1,'big')
        s+= len(algorithm_name).to_bytes(4,'big')+algorithm_name
        s+= len(pub_file).to_bytes(4,'big')+pub_file
        
        
        signature=self.sign(len(self.session_id).to_bytes(4,'big')+self.session_id+s, algorithm_name.decode('ascii'), self.key_list[self.current_key])
        
        self.serveur.send(s+len(signature).to_bytes(4,'big')+signature)
        
        if self.answer_publickey():
            return True;
        else:
            self.current_key = self.current_key+1
            if self.current_key >= len(self.key_list):
                self.auth_list.remove("publickey")
            return False
    
            
    def sign(self,s, algorithm_name, key_name):
        hash_str = SHA256.new(s)
        if algorithm_name == "ssh-rsa":
            with open(("client/keys/"+self.username+"/"+key_name), 'rb') as content_file:
                key = RSA.importKey(content_file.read())
                signature = pkcs1_15.new(key).sign(hash_str)
        elif algorithm_name == "ssh-dss":
            with open(("client/keys/"+self.username+"/"+key_name), 'rb') as content_file:
                key = DSA.importKey(content_file.read())
                signature = DSS.new(key, 'deterministic-rfc6979').sign(hash_str)
        else:
            print("clef de type non pris en charge")
            return False

            
        return base64.b64encode(signature)
            
    def Fillkeylist(self):
        if os.name=='nt':
            command= "dir client\\keys\\"+self.username
        else:
            command = "ls client/keys/"+self.username
    
        process= subprocess.Popen(command.split(), stdout= subprocess.PIPE, stderr= subprocess.PIPE)
        output, error = process.communicate()
    
        if error==b"":
            self.key_list = (output.decode()).split('\n')
            for x in self.key_list:
                if "pub" in x:
                    self.key_list.remove(x)
                    
            self.key_list.remove('')
            
        else:
            self.key_list = []
            self.auth_list.remove("publickey")

        
    def answer_none(self):
        s=self.serveur.recv(3000)
        if s[0]==SSH_MSG_USERAUTH_SUCCESS:
            return True
        
        elif s[0]==SSH_MSG_USERAUTH_FAILURE:
            length = int.from_bytes(s[1:5], 'big')
            self.auth_usable=(s[5:5+length].decode()).split(",")
            if s[5+length]==1:
                print("More authentification is required")
            return False
        else:
            print("Mauvaise requete")
            self.serveur.close()
            
    def answer_publickey(self):
        s=self.serveur.recv(3000)
        if s[0]==SSH_MSG_USERAUTH_SUCCESS:
            return SSH_MSG_USERAUTH_SUCCESS
        elif s[0]==SSH_MSG_USERAUTH_PK_OK:
            return SSH_MSG_USERAUTH_PK_OK
        
        elif s[0]==SSH_MSG_USERAUTH_FAILURE:
            length = int.from_bytes(s[1:5], 'big')
            self.auth_usable=(s[5:5+length].decode()).split(",")
            if s[5+length]==1:
                print("More authentification is required")
            return SSH_MSG_USERAUTH_FAILURE
        else:
            print("Mauvaise requete")
            self.serveur.close()
            
    def password_auth(self):
      
        passwd = input ("Enter password : ").encode('utf8')
        
        request = SSH_MSG_USERAUTH_REQUEST.to_bytes(1,'big')
            
        tmp = self.username.encode("utf8")
        request+=len(tmp).to_bytes(4,'big')+tmp
            
        tmp = "ssh-userauth".encode("ascii")
        request+=len(tmp).to_bytes(4,'big')+tmp
            
        tmp = "password".encode("ascii")
        request+=len(tmp).to_bytes(4,'big')+tmp
        
        a=0
        request+= a.to_bytes(1,'big')
        request+=len(passwd).to_bytes(4,'big')+passwd
    
        self.serveur.send(request)
        accepted = self.answer_password()
            
        while isinstance(accepted, str):
            print(accepted)
                
            old_passwd = input ("Enter old password : ").encode('utf8')
            new_passwd = input ("Enter new password : ").encode('utf8')
    
            request = SSH_MSG_USERAUTH_REQUEST.to_bytes(1,'big')
        
            tmp = self.username.encode("utf8")
            request+=len(tmp).to_bytes(4,'big')+tmp
        
            tmp = "ssh-userauth".encode("ascii")
            request+=len(tmp).to_bytes(4,'big')+tmp
            
            tmp = "password".encode("ascii")
            request+=len(tmp).to_bytes(4,'big')+tmp
            
            a=1
            request+= a.to_bytes(1,'big')
            request+=len(old_passwd).to_bytes(4,'big')+old_passwd
            request+=len(new_passwd).to_bytes(4,'big')+new_passwd
        
            self.serveur.send(request)
            accepted = self.answer_password()
        
        return accepted
            
            
    def answer_password(self):
        s=self.serveur.recv(3000)
        if s[0]==SSH_MSG_USERAUTH_SUCCESS:
            return True
        
        elif s[0]==SSH_MSG_USERAUTH_FAILURE:
            length = int.from_bytes(s[1:5], 'big')
            self.auth_usable=(s[5:5+length].decode()).split(",")
            if s[5+length]==1:
                print("More authentification is required")
            else:
                print("Fail")
            return False
        elif s[0]==SSH_MSG_USERAUTH_PASSWD_CHANGEREQ:
            length = int.from_bytes(s[1:5], 'big')
            l=s[5:5+length].decode("utf8")
            return l
        
        
        else:
            print("Mauvaise requete")
            self.serveur.close()


class Serveur:
    def __init__(self, client, session_id, auth_list=["publickey", "password"]):
        self.client = client
        self.client_username = ""
        self.session_id = session_id
        self.auth_list = auth_list
        
        self.key_list = []
        with open(("serveur/authorized_keys"), 'r') as content_file:
            self.key_list=content_file.read().split('\n')
        
        self.passwd_list = []
        with open(("serveur/passwd_list"), 'r') as content_file:
            self.passwd_list=content_file.read().split('\n')
            
        for i in range(len(self.passwd_list)):
            self.passwd_list[i] = self.passwd_list[i].split(" ")

        
        self.key_fail = 0
        self.passwd_fail=0
        
    def auth(self):
        while self.auth_list!=[]:
            if (self.request_received()):
                return True
        return False
            
        
        
    def request_received(self):
        s=self.client.recv((3000))
        l=decode_message(s)
        if l[0]!=SSH_MSG_USERAUTH_REQUEST:
            print("Mauvaise requete")
            self.client.close()
            
        else:
            if l[1]!=self.client_username:
                if self.client_username=="":
                    self.client_username = l[1]
                else: 
                    print("Mauvais client")
                    self.none()
                    return False
            
            if l[2]!="ssh-userauth":
                print("Mauvais service")
                self.none()
                return False
            
            elif l[3]=="publickey" and "publickey" in self.auth_list:
                return self.publickey(l)
            elif l[3]=="password" and "password" in self.auth_list:
                return self.password(l)
            else:
                return self.none()
                
    
    def none(self):
        request = SSH_MSG_USERAUTH_FAILURE.to_bytes(1,'big')
        l = ','.join(self.auth_list)
        bool = 0
        request+=len(l).to_bytes(4,'big')+l.encode("ascii")+bool.to_bytes(1,'big')
        
        self.client.send(request)
        
        return False
        
    def publickey(self,l):

        if not l[6] in self.key_list:
            self.key_fail+=1
            if self.key_fail >=10:
                self.auth_list.remove("publickey")
            self.none()
            return False
        else:
            request = SSH_MSG_USERAUTH_PK_OK.to_bytes(1,'big')
            request+=len(l[5]).to_bytes(4,'big')+l[5].encode('ascii')
            request+=len(l[6]).to_bytes(4,'big')+l[6].encode('ascii')
            
            self.client.send(request)
            
            s2=self.client.recv(3000)
            l2=decode_message(s2)
            
            #Verifie si c'est bien la bonne clef, le bon client etc
            if l[0:4]!=l2[0:4]:
                print("Mauvaise clef")
                self.key_fail+=1
                if self.key_fail >=10:
                    self.auth_list.remove("publickey")
                self.none()
                return False
            
            
            s = SSH_MSG_USERAUTH_REQUEST.to_bytes(1,'big')
            
            tmp = self.client_username.encode("utf8")
            s+=len(tmp).to_bytes(4,'big')+tmp
            tmp = "ssh-userauth".encode("ascii")
            s+=len(tmp).to_bytes(4,'big')+tmp
            tmp = "publickey".encode("ascii")
            s+=len(tmp).to_bytes(4,'big')+tmp
            a=1
            s+= a.to_bytes(1,'big')
            algorithm_name=l[5].encode('ascii')
            s+= len(algorithm_name).to_bytes(4,'big')+algorithm_name
            pub_file=l[6].encode('ascii')
            s+= len(pub_file).to_bytes(4,'big')+pub_file
                        
            plain_signature=len(self.session_id).to_bytes(4,'big')+self.session_id+s
            
            
            test = self.check_signature(l2[5], plain_signature, base64.b64decode(l2[7].encode('ascii')), l2[6])
            
            if test :
                request = SSH_MSG_USERAUTH_SUCCESS.to_bytes(1,'big')
                self.client.send(request)
                self.key_fail = 0
                return True
            else:
                self.key_fail+=1
                if self.key_fail >=10:
                    self.auth_list.remove("publickey")
                self.none()
                return False
        
    def check_signature(self, algorithm_name, plain_signature, signature, pubkey):
        hash_str = SHA256.new(plain_signature)
        try:
            if algorithm_name=="ssh-rsa":
                pub_key = RSA.importKey(pubkey)
                pkcs1_15.new(pub_key).verify(hash_str, signature)
                return True
            elif algorithm_name=="ssh-dss":
                pub_key = DSA.importKey(pubkey)
                DSS.new(pub_key, 'deterministic-rfc6979').verify(hash_str, signature)
                return True
            else:
                return False
            
        except ValueError:
            print("Mauvaise signature")
            return False
    
    def password(self,l):
        if self.passwd_fail>=3:
            self.auth_list.remove("password")
            self.none()
            return False
            
        hash_passwd = SHA256.new(l[5].encode('utf8')).hexdigest()
        if self.client_username != l[1] or l[2]!="ssh-userauth" or l[3]!="password" or l[4]==True:
            print("Wrong user or service name")
            self.none()
            self.passwd_fail+=1
            return False
        
        index = self.find_pass([self.client_username, hash_passwd])
        if index < 0:
            self.passwd_fail+=1
            if self.passwd_fail>=3:
                self.auth_list.remove("password")
            self.none()
            return False
        else:
            # Teste si le mot de passe a plus de 90 jours
            now = datetime.datetime.now()
            expiration = datetime.datetime.strptime(self.passwd_list[index][2], "%Y-%m-%d") + datetime.timedelta(days=90)
            if expiration < now:
                changed = False
                tries = 0
                while not changed and tries < 3:
                    request = SSH_MSG_USERAUTH_PASSWD_CHANGEREQ.to_bytes(1,'big')
                    prompt = "Expired password, please change it".encode("utf8")
                    language_tag = "en-US".encode('ascii')
                    request+=len(prompt).to_bytes(4,'big')+prompt
                    request+=len(language_tag).to_bytes(4,'big')+language_tag
                    self.client.send(request)
                    l2=decode_message(self.client.recv(3000))
                    if l2[0:4]!=l[0:4]:
                        tries==3
                        
                    elif l2[5]==l[5] and l2[5]!=l2[6]:
                        hash_passwd = SHA256.new(l2[6].encode('utf8')).hexdigest()
                        self.passwd_list[index][1]=hash_passwd
                        self.passwd_list[index][2]=now.strftime("%Y-%m-%d")
                        self.rewritePasswordFile()
                        changed=True
                    else:
                        tries+=1
                
                if tries < 3:
                    self.auth_list.remove("password")
                    request = SSH_MSG_USERAUTH_FAILURE.to_bytes(1,'big')
                    l = ','.join(self.auth_list)
                    bool = 1
                    request+=len(l).to_bytes(4,'big')+l.encode("ascii")+bool.to_bytes(1,'big')
        
                    self.client.send(request)
                    return False
                else:
                    self.auth_list.remove("password")
                    self.none()
                    return False
                    
            else:
                request = SSH_MSG_USERAUTH_SUCCESS.to_bytes(1,'big')
                self.client.send(request)
                return True
                
    def find_pass(self, l):
        for i in range(len(self.passwd_list)):
            if l[0]==self.passwd_list[i][0] and l[1]==self.passwd_list[i][1]:
                return i
        return -1
                
    def rewritePasswordFile(self):
        with open(("serveur/passwd_list"), 'w') as content_file:
            s=""
            for x in self.passwd_list:
                s+=" ".join(x)+"\n"
                
            content_file.write(s)
    
    def banner(self):
        with open(("serveur/banner"), 'r') as content_file:
            s = content_file.read().encode('utf8')
        
        request = SSH_MSG_USERAUTH_BANNER.to_bytes(1,'big')
        request+=len(s).to_bytes(4,'big')+s
        language_tag = "en-US".encode('ascii')
        request+=len(language_tag).to_bytes(4,'big')+language_tag
        self.client.send(request)
        
                    
                        
            

            
        
        
        

        
        
        
