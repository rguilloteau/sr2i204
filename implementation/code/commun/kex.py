if __name__ == "__main__":
    from sshconst import *
else:
    from commun.sshconst import *
    
import socket
    

class MyKex:
    
    def __init__(self, kex_algorithms = ["s"], server_host_key_algorithms = ["d"], encryption_algorithms_client_to_server = ["ed"], encryption_algorithms_server_to_client = ["sf"]):
        
        self.kex_algorithms = kex_algorithms
        self.server_host_key_algorithms = server_host_key_algorithms
        self.encryption_algorithms_client_to_server = encryption_algorithms_client_to_server
        self.encryption_algorithms_server_to_client = encryption_algorithms_server_to_client
    

    def send_init(self,dest):
        
        kex_algorithms_string = ""
        server_host_key_algorithms_string = ""
        encryption_algorithms_client_to_server_string = ""
        encryption_algorithms_server_to_client_string = ""

        for x in self.kex_algorithms:
            kex_algorithms_string+=x+","
        kex_algorithms_string= kex_algorithms_string[:-1]

        for x in self.server_host_key_algorithms:
            server_host_key_algorithms_string+=x+","
        server_host_key_algorithms_string= server_host_key_algorithms_string[:-1]
        
        for x in self.encryption_algorithms_client_to_server:
            encryption_algorithms_client_to_server_string+=x+","
        encryption_algorithms_client_to_server_string= encryption_algorithms_client_to_server_string[:-1]
        
        for x in self.encryption_algorithms_server_to_client:
            encryption_algorithms_server_to_client_string+=x+","
        encryption_algorithms_server_to_client_string= encryption_algorithms_server_to_client_string[:-1]
        
        res = format(KEY_EXCHANGE_INIT,'04x')
        res+= format(len(kex_algorithms_string),'04x')+kex_algorithms_string
        res+= format(len(server_host_key_algorithms_string),'04x')+server_host_key_algorithms_string
        res+= format(len(encryption_algorithms_client_to_server_string),'04x')+encryption_algorithms_client_to_server_string
        res+= format(len(encryption_algorithms_server_to_client_string),'04x')+encryption_algorithms_server_to_client_string
    
        dest.send(res.encode())
            

class ItsKex:
    
    def __init__(self):
        self.kex_algorithms = []
        self.server_host_key_algorithms = []
        self.encryption_algorithms_client_to_server = []
        self.encryption_algorithms_server_to_client = []
        
 
    def receive_init(self,sender):
        
        s=(sender.recv(3000)).decode()
        
        
        if s[0:4]!=format(KEY_EXCHANGE_INIT,'04x'):
            return False
        else:
            s=s[4:]
            
            l=int(s[0:4],16)
            s=s[4:]
            self.kex_algorithms=s[0:l].split(',')
            s=s[l:]
            
            l=int(s[0:4],16)
            s=s[4:]
            self.server_host_key_algorithms=s[0:l].split(',')
            s=s[l:]
            
            l=int(s[0:4],16)
            s=s[4:]
            self.encryption_algorithms_client_to_server=s[0:l].split(',')
            s=s[l:]
            
            l=int(s[0:4],16)
            s=s[4:]
            self.encryption_algorithms_server_to_client=s[0:l].split(',')
            s=s[l:]
            
            if len(s)!=0:
                return False
            return True
            
            
class ProtocolKex:
    def __init__(self):
        self.kex_algorithms_used = ""
        self.server_host_key_algorithms_used = ""
        self.encryption_algorithms_client_to_server_used = ""
        self.encryption_algorithms_server_to_client_used = ""
    
    def choice(self, clientKex, serveurKex):
        #On parcours chacune des listes de client jusqu'à trouver un élément aussi présent dans serveurKex
        
        for x in clientKex.kex_algorithms:
            if x in serveurKex.kex_algorithms:
                self.kex_algorithms_used=x
                break
        if self.kex_algorithms_used=="":
            print("kex_algorithms_used")
            return False
        
        for x in clientKex.server_host_key_algorithms:
            if x in serveurKex.server_host_key_algorithms:
                self.server_host_key_algorithms_used=x
                break
        if self.server_host_key_algorithms_used=="":
            print("server_host_key_algorithms_used")
            return False
            
        for x in clientKex.encryption_algorithms_client_to_server:
            if x in serveurKex.encryption_algorithms_client_to_server:
                self.encryption_algorithms_client_to_server_used=x
                break
        if self.encryption_algorithms_client_to_server_used=="":
            print("encryption_algorithms_client_to_server_used")
            return False
            
        for x in clientKex.encryption_algorithms_server_to_client:
            if x in serveurKex.encryption_algorithms_server_to_client:
                self.encryption_algorithms_server_to_client_used=x
                break
        if self.encryption_algorithms_server_to_client_used=="":
            print("encryption_algorithms_server_to_client_used")
            return False
        
        return True