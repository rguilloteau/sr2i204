# Bibiliographie

1. [IETF, _The Secure Shell (SSH) Protocol Architecture (rfc4521)_](https://www.ietf.org/rfc/rfc4251)

2. [IETF, _The Secure Shell (SSH) Authentication Protocol (rfc4252)_](https://www.ietf.org/rfc/rfc4252)


3. [IETF, _The Secure Shell (SSH) Transport Layer Protocol (rfc4253)_](https://www.ietf.org/rfc/rfc4253)


4. [IETF, _The Secure Shell (SSH) Connection Protocol (rfc4254)_](https://www.ietf.org/rfc/rfc4254)


5. [Aghiles, _Cisco - Implémenter SSH sur un Firewall_](https://aghiles.fr/implementer-ssh-sur-un-firewall-cisco/)


6. [Jean Rohmer. _SSH : un outil et des techniques simples à implémenter pour construire et simuler des
modèles hiérarchisés de systèmes.  Modélisation et simulation._  Institut National Polytechnique de Grenoble - INPG, 1976. Français.](https://tel.archives-ouvertes.fr/tel-00287086/document)


7. [Barrett Daniel J.et Silverman Richard E. et Jacoboni Éric, _SSH le shell sécurisé : la référence / Daniel J. Barrett & Richard E. Silverman ; traduction d'Eric Jacoboni_, O'Reilly](https://telecomparistech.focus.universite-paris-saclay.fr/primo-explore/fulldisplay?docid=33IMT_ILS16560&context=L&vid=33PTECH_VU1&search_scope=33PTECH2&isFrbr=true&tab=default_tab&lang=fr_FR)

8. [Tatu Ylonen, _SSH (Secure Shell)_](https://www.ssh.com/ssh/)
