# Implémentation de l'authentification pour SSHv2

## Implémentation actuelle

Nous avons décidé de coder en Python car cela permet au programme d'être utilisé à la fois sur une machine Linux et une machine sous OS propriétaire. De plus, il est très simple d'envoyer des requêtes TCP grâce aux sockets.

Pour cette première implémentation, nous avons décidé de faire une version simplifiée de l'authentification. En effet, pour l'instant, notre implémentation ne supporte que RSA pour le chiffrement à clé publique et AES-128 pour le chiffrement symétrique. Cette première version propose cependant une authentification du serveur, la mise en place d'un canal sécurisé et l'authentification du client.

Il faut notamment que l'utilisateur remplisse lui-même les fichiers contenants les clés ssh autorisées si il veut l'utiliser avec une adresse IP différente de `127.0.0.1` car seule cette adresse IP est rentrée.

De plus, il est préférable d'éxécuter ces programmes avec Pyzo pour l'instant car il est possible d'avoir des problèmes d'encodage de caractères non ASCII comme avec Spyder par exemple.

### Code du client

```Python

import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
import os
import base64


def check_identity(hote,key):
    print("Check identity")
    with open(("known_hosts"), 'r') as content_file:
        list=content_file.read()
    return (hote+" "+key in list)

BLOCK_SIZE = 16

# caractère utilisé pour le padding afin que la taille de la chaine envoyée avec aes soit un multiple de BLOCK_SIZE
PADDING = b'{'

# Padding de la chaine à envoyer
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING

EncodeAES = lambda c, s: c.encrypt(pad(s))
DecodeAES = lambda c, e: c.decrypt(e).rstrip(PADDING)

## Client Main
import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
import os
import base64


def check_identity(hote,key):
    print("Check identity")
    with open(("known_hosts"), 'r') as content_file:
        list=content_file.read()
    return (hote+" "+key in list)

BLOCK_SIZE = 16

# caractère utilisé pour le padding afin que la taille de la chaine envoyée avec aes soit un multiple de BLOCK_SIZE
PADDING = b'{'

# Padding de la chaine à envoyer
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING

EncodeAES = lambda c, s: c.encrypt(pad(s))
DecodeAES = lambda c, e: c.decrypt(e).rstrip(PADDING)

## Client Main

hote = input("IP de l'hôte : ")

port = 9999

client = input("Nom du client : ")

with open(("private_"+client+".key"), 'rb') as content_file:
    key=RSA.importKey(content_file.read())

serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

serveur.connect((hote, port))

print("Connexion avec le serveur sur le port {}".format(port))

#Réception de la clé publique du serveur
key_str = (serveur.recv(3000)).decode()
public_serveur_key=RSA.importKey(key_str)

#Vérification de l'identité du serveur
if not check_identity(hote, key_str):
    serveur.close()
else:
    aes_key = os.urandom(BLOCK_SIZE)
    aes_key_crypted = public_serveur_key.encrypt(aes_key,1)[0]
    serveur.send(aes_key_crypted)
    aes_cipher = AES.new(aes_key)

    encoded = EncodeAES(aes_cipher, (client+" ").encode()+key.exportKey('OpenSSH'))
    serveur.send(encoded)

    while True:

        message = input('Message à envoyer : ')
        encoded = EncodeAES(aes_cipher, message.encode())
        serveur.send(encoded)

```

### Code du serveur

```Python

import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
import base64


def check_identity(hote,key):
    print("Check identity")
    with open(("keys_database"), 'r') as content_file:
        list=content_file.read()
    return (hote+" "+key in list)

BLOCK_SIZE = 16

# caractère utilisé pour le padding afin que la taille de la chaine envoyée avec aes soit un multiple de BLOCK_SIZE
PADDING = b'{'

# Padding de la chaine à envoyer
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING

EncodeAES = lambda c, s: c.encrypt(pad(s))
DecodeAES = lambda c, e: c.decrypt(e).rstrip(PADDING)

## Main Serveur

address = input("Adresse d'écoute : ")

with open("private.key", 'rb') as content_file:
    key=RSA.importKey(content_file.read())

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.bind((address, 9999))

while True:
        socket.listen(5)
        client, address = socket.accept()
        print("{} connected".format( address ))

        # Envoie de la clé publique du serveur au client pour être authentifié
        client.send(key.exportKey('OpenSSH'))

        #Réception de
        aes_key_crypted = (client.recv(1000))

        aes_key = key.decrypt(aes_key_crypted)

        aes_cipher = AES.new(aes_key)

        encoded = client.recv(3000)
        client_key = DecodeAES(aes_cipher, encoded)
        print(client_key)
        if not check_identity(address[0], client_key.decode()):
            client.close()
            socket.close()
            break
        else:
            while True:
                encoded = client.recv(3000)
                decoded = DecodeAES(aes_cipher, encoded)
                print("Message reçu : "+decoded.decode())

```
----

## Prochains objectifs

Cette version ne respecte pas du tout les normes imposées par l'IEEE. Nous chercherons donc à implémenter correctement les requêtes en respectant le format imposé.

De plus, nous allons élargir le nombre de protocoles de chiffrement supportés comme `ecdsa-sha2-nistp256` qui est utilisé par les machines de l'école pour le chiffrement asymétrique.

Nous allons aussi augmenter le nombre possible de techniques pour authentifier le client comme le host-based ou par mot de passe. Ainsi il faut aussi mettre en place la gestion du choix de la méthode d'authentification.


Nous allons aussi mettre en place l'échange de clé avec la méthode Diffie - Hellman pour la création du canal sécurisé.

Nous allons étudié aussi plus en détail le Digital Signature Algorithm qui est un algorithme de signature ajouté à SSH dans la version 2.

De plus, nous allons implémenter des techniques en réponse aux attaques comme par exemple en limitant le nombre de tentatives de connexion avant de mettre le serveur hors ligne.
