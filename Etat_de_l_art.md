# Etude, analyse et mise en œuvre du protocole d’authentification mis en œuvre dans SSHv2

## État de l'art par Romain Guilloteau et Jacques Wilmé

___

## Introduction


Le Secure Shell (SSH) Protocol est un protocole de connexion sécurisée à distance entre un client et un serveur. Il permet de réaliser des communications sécurisées et intègres grâce à de l'encryption forte à travers plusieurs techniques de connexion dont la plus répandue est celle à clé publique. Ce protocole est une alternative aux protocoles de connexion et aux transferts de fichier non-sécurisés comme telnet ou FTP.

SSH possède deux versions : SSH1 et SSH2. La première a été créée par Tatu Ylönen en 1995 pour sécuriser les serveurs de son université à Helsinki après une attaque dans laquelle tous les mots de passe ont été volés. Cependant cette version comporte de nombreuses failles qui ont été corrigées dans SSH2. Même si elles portent le même nom, les deux versions sont incompatibles entre elles car la réparation de ces failles a poussé à modifier de manière profonde le protocole qui a donc de nombreuses différences entre les deux versions. La deuxième est donc aussi beaucoup plus sécurisée.

Dans notre projet, nous allons nous attarder sur la partie authentification du protocole de SSHv2. Nous ne traiterons pas du transfert de fichier (SFTP), ni le transfert de port (comme X11).


## Le protocole de connexion

Le protocole de connexion se déroule en plusieurs étapes comme l'explique le site de [SSH Communications Security](https://www.ssh.com/ssh/protocol/):

![Schéma de la connexion](images/how-does-ssh-protocol-work.png)

1. **Le client commence la connexion en contactant le serveur**


2. **Le serveur répond en envoyant sa clé publique pour s'authentifier**

   Le serveur envoie au client sa clé publique au client. Ce dernier va comparer la clé publique reçue avec l'adresse de l'hôte avec la liste des hôtes connus pour éviter une attaque de l'homme du milieu. Ce procédé est expliqué plus en détail ainsi que d'autres failles de SSH dans le livre [SSH le shell sécurisé : la référence](https://telecomparistech.focus.universite-paris-saclay.fr/primo-explore/fulldisplay?docid=33IMT_ILS16560&context=L&vid=33PTECH_VU1&search_scope=33PTECH2&isFrbr=true&tab=default_tab&lang=fr_FR)

3. **Le client et le serveur ouvrent un canal sécurisé entre eux et se mettent d'accord sur le protocole d'authentification du client**

   Les deux entités mettent en place un canal sécurisé par un chiffrement symétrique puis décident du protocole utilisé pour authentifier le client. Il en existe plusieurs et comme l'explique le document [RFC4252](https://www.ietf.org/rfc/rfc4252) seul le chiffrement asymétrique est obligatoire. Le client devra choisir parmi tous ceux proposés par le serveur qui peut aussi implémenter le système de mot de passe ou de Host-based Authentification.

4. **Enfin le client tente de se connecter grâce au protocole utilisé**

___
## Bibiliographie

1. [IETF, _The Secure Shell (SSH) Protocol Architecture (rfc4521)_](https://www.ietf.org/rfc/rfc4251)

2. [IETF, _The Secure Shell (SSH) Authentication Protocol (rfc4252)_](https://www.ietf.org/rfc/rfc4252)


3. [IETF, _The Secure Shell (SSH) Transport Layer Protocol (rfc4253)_](https://www.ietf.org/rfc/rfc4253)


4. [IETF, _The Secure Shell (SSH) Connection Protocol (rfc4254)_](https://www.ietf.org/rfc/rfc4254)


5. [Aghiles, _Cisco - Implémenter SSH sur un Firewall_](https://aghiles.fr/implementer-ssh-sur-un-firewall-cisco/)


6. [Jean Rohmer. _SSH : un outil et des techniques simples à implémenter pour construire et simuler des
modèles hiérarchisés de systèmes.  Modélisation et simulation._  Institut National Polytechnique de Grenoble - INPG, 1976. Français.](https://tel.archives-ouvertes.fr/tel-00287086/document)


7. [Barrett Daniel J.et Silverman Richard E. et Jacoboni Éric, _SSH le shell sécurisé : la référence / Daniel J. Barrett & Richard E. Silverman ; traduction d'Eric Jacoboni_, O'Reilly](https://telecomparistech.focus.universite-paris-saclay.fr/primo-explore/fulldisplay?docid=33IMT_ILS16560&context=L&vid=33PTECH_VU1&search_scope=33PTECH2&isFrbr=true&tab=default_tab&lang=fr_FR)

8. [Tatu Ylonen, _SSH (Secure Shell)_](https://www.ssh.com/ssh/)
